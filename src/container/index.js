import React from 'react'
import { Header, Toastr } from '../components'

const Index = props => {
  return (
    <div>
      <Header />
      <Toastr />
      <main className="page">
        <section className="page__content">
          <div className="container">
            {props.children}
          </div>
        </section>
      </main>
    </div>
  )
}

export default Index;