import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Logo } from '../../assets/images/'
import { getLogin, setLogin } from '../../actions/login-action'
import { Toastr } from '../../components'
import Auth from '../../security/auth'
import '../../assets/proa/css/pages/login.css'

class Login extends Component {
  onLogin(event) {
    event.preventDefault()

    const username = this.refs.username
    const password = this.refs.password
    const credential = { login: username.value.trim(), password: btoa(password.value.trim()) }
    this.props.getLogin(credential)
  }

  componentWillReceiveProps(nextProps) {
    if (Auth.isLoggedIn()) {
      this.props.history.push('/')
    }
  }

  render() {
    return (
      <div>
        <main className="page">
          <Toastr/>
          <section className="login">
            <div className="login__container">
              <div className="login__box -slogan">
                <img src={Logo} alt="Magazine Luiza - Administrativo" className="navbar__brand__logo" />
                <span className="login__powered-by">Desenvolvido por Luizalabs</span>
              </div>
              <div className="login__box -form">
                <h2 className="login__title">Acesso ao sistema</h2>
                <form className="form" onSubmit={(event) => this.onLogin(event)}>
                  <div className="form-input">
                    <label htmlFor="username" className="form-input__label">Usuário</label>
                    <input type="text" id="username" name="username" ref="username" placeholder="Digite seu usuário" className="form-input__field" autoFocus />
                    <span className="form-input__help-text">Insira seu usuário do magazine luiza</span>
                  </div>
                  <div className="form-input">
                    <label htmlFor="password" className="form-input__label">Senha</label>
                    <input type="password" id="password" name="password" ref="password" placeholder="Digite sua senha" className="form-input__field" />
                    <span className="form-input__help-text">Senha do sistema do magazine luiza</span>
                  </div>
                  <button type="submit" className="button -persistence -block">Acessar</button>
                </form>
              </div>
            </div>
          </section>
        </main>
      </div>
    )
  }
}

const mapStateToProps = store => {
  return {
    credential: store.LoginReducer.credential
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getLogin: bindActionCreators(getLogin, dispatch),
    setLogin: bindActionCreators(setLogin, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)