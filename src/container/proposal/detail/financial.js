import React from 'react'
import { Panel, LabelText, LabelValue } from '../../../components'
import { FinancialIcon } from '../../../assets/images'

const Financial = props => {
  const proposal = props.proposal

  return (
    <Panel icon={FinancialIcon} subTitle={'Financeira'}>
      <div>
        <LabelText text={proposal.financial.description} />
      </div>
      <div>
        {
          (proposal.validator && proposal.validator.calculated)
          && (
            <div>
              <LabelText text={`SCORE: ${proposal.validator.calculated.code}`} />
            </div>
          )
        }
      </div>
      {
        proposal.validator &&
        <div>
          <div>
            <LabelValue text={`Liberado ${(proposal.validator.calculated.released) ? 'automaticamente' : 'manualmente'}`} />
          </div>
          <div>
            <LabelValue text={`Formalização ${(proposal.formalization.type === 'digital') ? 'digital' : 'tradicional'}`} />
          </div>
        </div>
      }
      <div>
      </div>
      {
        (proposal.validator && proposal.validator.actual)
        && (
          <div>
            <LabelText text={'Liberador:'} />
            <LabelValue text={`${proposal.validator.actual.username} - ${proposal.validator.actual.level}`} />
          </div>
        )
      }
    </Panel>
  )
}

export default Financial