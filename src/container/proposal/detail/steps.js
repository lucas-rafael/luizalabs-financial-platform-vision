import React from 'react'
import moment from 'moment'
import { Panel } from '../../../components'

const getColor = (status) => {
  if (status === 1)
    return ''
  if (status === 2)
    return '-green'
  return '-red'
}

const Steps = props => {
  const steps = props.steps

  return (
    <Panel>
      <ul>
        {
          Array.isArray(steps) &&
          (
            steps.map((item, index) =>
              <li className={`text ${getColor(item.type.id)}`} key={index}>{moment(item.date).format('DD/MM/YYYY hh:mm:ss')} - {item.description} ({item.event.description})</li>
            )
          )
        }
      </ul>
    </Panel>
  )
}

export default Steps