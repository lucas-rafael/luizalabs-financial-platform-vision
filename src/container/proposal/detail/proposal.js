import React from 'react'
import moment from 'moment'
import _ from 'lodash'
import { Panel, LabelText, LabelValue, LabelStrong } from '../../../components'
import { proposalStatus } from '../../../constants'

const Proposal = props => {
  const proposal = props.proposal

  let proposalColor = ''

  if (_.find(proposalStatus.STATUS_OK, (x) => x.id === proposal.status.id)) {
    proposalColor = '-green'
  } else if (_.find(proposalStatus.STATUS_ERROR, (x) => x.id === proposal.status.id)) {
    proposalColor = '-red'
  } else if (_.find(proposalStatus.STATUS_WARNING, (x) => x.id === proposal.status.id)) {
    proposalColor = '-orange'
  }

  return (
    <Panel title={`#${proposal.id}`} subTitle={moment(proposal.date).format('DD/MM/YYYY')}>
      <div>
        <LabelStrong text={`${proposal.status.id} - ${proposal.status.description}`} className={`label-strong text ${proposalColor}`}/>
      </div>
      <div>
        <LabelText text='Integration Code:' />
        <LabelValue text={proposal.customer.integrationCode} />
      </div>
      <div>
        <LabelText text='Operação:' />
        <LabelValue text={`R$ ${proposal.operationValue.toFixed(2)}`} />
      </div>
      <div>
        <LabelText text='Financiado:' />
        <LabelValue text={`R$ ${proposal.financedValue.toFixed(2)}`} />
      </div>
      {
        (proposal.payment && proposal.payment.installments && proposal.payment.installments.length > 0)
        && (
          <div>
            <div>
              <LabelText text={'1ª Parcela: '} />
              <LabelValue text={moment(proposal.payment.installments[0].first).format('DD/MM/YYYY')} />
            </div>
            <div>
              <LabelText text='Parcelas:' />
              <LabelValue text={`${proposal.payment.installments[0].count} x R$ ${proposal.payment.installments[0].value.toFixed(2)}`} />
            </div>
          </div>
        )
      }
    </Panel>
  )
}

export default Proposal