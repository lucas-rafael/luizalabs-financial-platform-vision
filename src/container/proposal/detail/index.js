import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Row, Col } from '../../../components'
import Proposal from './proposal'
import Customer from './customer'
import Salesman from './salesman'
import Financial from './financial'
import Antifraud from './antifraud'
import Payload from './payload'
import Steps from './steps'
import { getProposal, getDocuments, getSteps } from '../../../actions/proposal-action'

class ProposalDetail extends Component {
  componentDidMount() {
    const id = this.props.match.params.id
    if (!isNaN(id)) {
      this.props.getProposal(id)
      this.props.getDocuments(id)
      this.props.getSteps(id)
    }
  }

  render() {
    const proposal = this.props.proposal
    const documents = this.props.documents
    const steps = this.props.steps

    return (
      <div>
        {
          (proposal && Object.keys(proposal).length > 0) ?
            <div>
              <section>
                <Row>
                  <Col size={12}>
                    <h1 className='nav-panel__title'>Proposta</h1>
                  </Col>
                </Row>
                <Row>
                  <Col size={3}>
                    <Proposal proposal={proposal} />
                  </Col>
                  <Col size={3}>
                    <Customer proposal={proposal} />
                  </Col>
                  <Col size={3}>
                    <Salesman proposal={proposal} />
                  </Col>
                  <Col size={3}>
                    {
                      proposal.financial &&
                      <Financial proposal={proposal} />
                    }
                  </Col>
                </Row>
              </section>
              {
                proposal.antiFraud &&
                <section className={`box ${(documents && documents.length > 0) ? '' : 'box-hidden'}`}>
                  <Row>
                    <Col size={12}>
                      <h1 className='nav-panel__title'>Anti-fraude</h1>
                    </Col>
                  </Row>
                  <Row>
                    <Col size={12}>
                      <Antifraud proposal={proposal} documents={documents} />
                    </Col>
                  </Row>
                </section>
              }

              <section className={`box ${(steps && steps.length > 0) ? '' : 'box-hidden'}`}>
                <Row>
                  <Col size={12}>
                    <h1 className='nav-panel__title'>Auditoria</h1>
                  </Col>
                </Row>
                <Row>
                  <Col size={12}>
                    <Steps steps={steps} />
                  </Col>
                </Row>
              </section>
              
              <section>
                <Row>
                  <Col size={12}>
                    <h1 className='nav-panel__title'>Payload</h1>
                  </Col>
                </Row>
                <Row>
                  <Col size={12}>
                    <Payload payload={proposal} />
                  </Col>
                </Row>
              </section>
            </div>
          :
          <p>Proposta não encontrada.</p>
        }
      </div>
    )
  }
}

const mapStateToProps = store => {
  return ({
    proposal: store.ProposalReducer.proposal,
    documents: store.ProposalReducer.documents,
    steps: store.ProposalReducer.steps,
  })
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProposal: bindActionCreators(getProposal, dispatch),
    getDocuments: bindActionCreators(getDocuments, dispatch),
    getSteps: bindActionCreators(getSteps, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProposalDetail)