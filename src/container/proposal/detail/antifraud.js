import React from 'react'
import { Panel, Thumbnail } from '../../../components'

const Antifraud = props => {
  const proposal = props.proposal
  const documents = props.documents

  return (
    <Panel title={`#${proposal.antiFraud.id}`}>
      {
        Array.isArray(documents) && 
        (
          documents.map((item, index) => 
            <Thumbnail key={index} image={item} />
          )
        )
      }
    </Panel>
  )
}

export default Antifraud