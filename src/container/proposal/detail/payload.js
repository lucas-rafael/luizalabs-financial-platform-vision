import React from 'react'
import ReactJson from 'react-json-view'
import { Panel } from '../../../components'

const Payload = props => {
    return (
        <Panel>
            <ReactJson 
                src={props.payload} 
                name={'payload'} 
                collapsed={1}
                displayDataTypes={false}
                displayObjectSize={false} />
        </Panel>
    )
}

export default Payload