import React from 'react'
import { Panel, LabelText, LabelValue } from '../../../components'
import { SalesmanIcon } from '../../../assets/images'

const Salesman = props => {
  const proposal = props.proposal

  return (
    <Panel icon={SalesmanIcon} subTitle={'Vendedor'}>
      <LabelText text={proposal.salesman.name} />
      <div>
        <LabelText text='ID:' />
        <LabelValue text={proposal.salesman.userId} />
      </div>
      <div>
        <LabelText text='CPF:' />
        <LabelValue text={proposal.salesman.cpf} />
      </div>
      <div>
        <LabelText text='Filial:' />
        <LabelValue text={proposal.branch.id} />
      </div>
    </Panel>
  )
}

export default Salesman