import React from 'react'
import moment from 'moment'
import { Panel, LabelText, LabelValue } from '../../../components'
import { CustomerIcon } from '../../../assets/images'

const Customer = props => {
  const proposal = props.proposal

  return (
    <Panel icon={CustomerIcon} subTitle={'Cliente'}>
      <LabelText text={proposal.customer.name} />
      <div>
        <LabelText text='ID:' />
        <LabelValue text={proposal.customer.integrationCode} />
      </div>
      {
        proposal.customer.documents
        && (
          proposal.customer.documents.map((document, index) =>
            <div key={index}>
              <LabelText text={`${document.type}:`} />
              <LabelValue text={document.id} />
            </div>)
        )
      }
      <div>
        <LabelText text='Nascimento:' />
        <LabelValue text={moment(proposal.customer.birth.date).format('DD/MM/YYYY')} />
      </div>
    </Panel>
  )
}

export default Customer