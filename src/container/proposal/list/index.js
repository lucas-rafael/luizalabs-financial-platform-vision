import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'
import moment from 'moment'
import _ from 'lodash'
import Search from './search'
import { Table, Pagination } from '../../../components'
import { proposalStatus, routes } from '../../../constants'
import { getProposals } from '../../../actions/proposal-action'

class ProposalList extends Component {
  componentDidMount() {
    this.onChangePage()
  }

  onChangePage = (offset = 0, limit = 50) => {
    this.props.getProposals(offset, limit, this.props.filters)
  }

  goToProposalDetail = idProposal => {
    this.props.history.push(`${routes.PROPOSAL_LIST.path}/${idProposal}`)
  }

  render() {
    let proposals = this.props.proposals || []

    return (
      <div>
        <Search onDetail={this.goToProposalDetail} getProposals={(offset, limit, filters) => this.props.getProposals(offset, limit, filters)} filters={this.props.filters} />
        {
          proposals.records ?
            <div>
              <Table striped>
                <Table.Head>
                  <Table.Row>
                    <Table.HeadCell className={'align-center'}>ID</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>STATUS</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>DATA</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>FILIAL</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>VENDEDOR</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>CPF CLIENTE</Table.HeadCell>
                    <Table.HeadCell className={'align-center'}>FINANCEIRA</Table.HeadCell>
                  </Table.Row>
                </Table.Head>
                <Table.Body>
                  {
                    proposals.records.map((item, index) => {
                      let proposalColor = ''
                      let clientCPF = ''
                      let maskCPF = '';
                      let indexMask = 0;

                      if (_.find(proposalStatus.STATUS_OK, (x) => x.id === item.status.id)) {
                        proposalColor = '-green'
                      } else if (_.find(proposalStatus.STATUS_ERROR, (x) => x.id === item.status.id)) {
                        proposalColor = '-red'
                      } else if (_.find(proposalStatus.STATUS_WARNING, (x) => x.id === item.status.id)) {
                        proposalColor = '-orange'
                      } else {
                        proposalColor = '-default'
                      }

                      item.customer.documents.map((document, index) => {
                        if (document.type === 'CPF') {
                          clientCPF = document.id
                          return true
                        }
                        return false
                      })

                      _.forEach('999.999.999-99', function (char) {
                        if (char === '9') {
                          maskCPF += clientCPF[indexMask];
                          indexMask++;
                        } else {
                          maskCPF += char;
                        }
                      })

                      return (
                        <Table.Row key={index}>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{item.id}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={`text ${proposalColor}`}>{item.status.id} - {item.status.description}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{moment(item.date).format('DD/MM/YYYY')}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{item.branch.id}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{item.salesman.userId}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{maskCPF}</Link></Table.Cell>
                          <Table.Cell className={'align-center'}><Link to={`/cdc/proposals/${item.id}`} className={'text -default'}>{item.financial ? item.financial.description : null}</Link></Table.Cell>
                        </Table.Row>
                      )
                    })
                  }
                </Table.Body>
              </Table>
              <Pagination
                limit={proposals.meta.limit}
                offset={proposals.meta.offset}
                recordCount={proposals.meta.recordCount}
                onChangePage={(offset, limit) => this.onChangePage(offset, limit)} />
            </div>
          :
            <p>Nenhum registro encontrado.</p>
        }
      </div>
    )
  }
}

const mapStateToProps = store => {
  return ({
    proposals: store.ProposalReducer.proposals,
    filters: store.ProposalReducer.filters,
  })
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProposals: bindActionCreators(getProposals, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProposalList)