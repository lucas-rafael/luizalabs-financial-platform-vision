import React, { Component } from 'react'
import _ from 'lodash'
import { typeSearch } from '../../../constants'


class Search extends Component {
    constructor(props) {
        super(props)

        this.state = {
            formData: {
                typeSearch: typeSearch.ID_PROPOSAL,
                textSearch: ''
            }
        }

        this.serch = this.serch.bind(this)
        this.inputChange = this.inputChange.bind(this)
    }

    serch(event) {
        event.preventDefault()

        const type = this.state.formData.typeSearch
        const text = this.state.formData.textSearch

        let filters = this.props.filters
        if (type === typeSearch.STATUS_PROPOSAL) {
            filters = this.pushFilter(filters, 'Status', `&status.id=${text}`, text)
        } else if (type === typeSearch.CPF_CUSTUMER) {
            filters = this.pushFilter(filters, 'CPF', `&customer.documents%5B%5D.type=CPF&customer.documents%5B%5D.value=${text}`, text)
        } else if (type === typeSearch.ID_FINANCIAL) {
            filters = this.pushFilter(filters, 'Financeira', `&financial.id=${text}`, text)
        } else {
            this.props.onDetail(text)
            return false
        }

        this.props.getProposals(0, 50, filters)
    }

    pushFilter (filters, name, queryString, value) {
        _.remove(filters, (x) => x.name === name)

        filters.push({ name, queryString, value })
        return filters
    }

    inputChange(event) {
        let formData = Object.assign({}, this.state.formData);
        formData[event.target.name] = event.target.value;
        this.setState({ formData })
    }

    removeFilter(name) {
        let filters = this.props.filters
        _.remove(filters, (x) => x.name === name)
        this.props.getProposals(0, 50, filters)
    }

    render() {
        return (
            <div className="search">
                <form className="form" onSubmit={this.serch}>
                    <div className="form-input -row">
                        <select name="typeSearch" id="typeSearch" className="form-input__select" onChange={this.inputChange}>
                            <option defaultValue={typeSearch.ID_PROPOSAL}>ID da proposta</option>
                            <option value={typeSearch.CPF_CUSTUMER}>CPF do cliente</option>
                            <option value={typeSearch.STATUS_PROPOSAL}>Status da proposta</option>
                            <option value={typeSearch.ID_FINANCIAL}>ID da financeira</option>
                        </select>
                        <input type="text" name="textSearch" id="textSearch" placeholder="Procure pela proposta" className="form-input__field" onChange={this.inputChange} />
                        <button type="submit" className="button">Buscar</button>  
                    </div>
                </form>
                <div className="tags">
                    {
                        this.props.filters.map((item, index) => {
                            return <span key={index} className="tag">{item.name}: {item.value} <i className="removeTag" onClick={() => this.removeFilter(item.name)}>&times;</i></span>
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Search