import ReduxThunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import ProposalReducer from './proposal-reducer'
import LoginReducer from './login-reducer'

const store = createStore(
  combineReducers(
    {
      ProposalReducer,
      LoginReducer
    }
  ),
  {},
  applyMiddleware(ReduxThunk)
)

export default store;
