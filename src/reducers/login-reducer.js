import { SET_LOGIN } from '../constants/action-types';

const INITIAL_STATE = {
  credential: null,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_LOGIN:
      return { ...state, credential: action.payload }
    default:
      return state;
  }
}