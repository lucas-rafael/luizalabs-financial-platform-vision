import { SET_PROPOSALS, SET_PROPOSAL, SET_DOCUMENTS, SET_STEPS } from '../constants/action-types';

const INITIAL_STATE = {
  proposals: null,
  proposal: null,
  documents: null,
  steps: null,
  filters: [],
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_PROPOSALS:
      return { ...state, proposals: action.payload, filters: action.filters }
    case SET_PROPOSAL:
      return { ...state, proposal: action.payload }
    case SET_DOCUMENTS:
      return { ...state, documents: action.payload }
    case SET_STEPS:
      return { ...state, steps: action.payload }
    default:
      return state;
  }
}