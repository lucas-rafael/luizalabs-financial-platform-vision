import { LoginService } from '../services'
import { SET_LOGIN } from '../constants/action-types'
import { Errors } from '../helpers'
import Auth from '../security/auth'

export const getLogin = credential => {
  return async dispatch => {
    const loginService = new LoginService()
    let result = null

    try {
      result = await loginService.login(credential)

      Auth.logIn(result.data.records[0])
      dispatch(setLogin(result.data.records[0]))
    } catch (error) {
      Errors.catchHttp(error, [{
        statusCode: 400,
        title: 'Autenticação Falhou!',
        message: 'Preencha o usuário e a senha'
      }, {
        statusCode: 401,
        title: 'Autenticação Falhou!',
        message: 'Usuário não autorizado'
      }, {
        statusCode: 404,
        title: 'Autenticação Falhou!',
        message: 'Usuário ou senha incorretos'
      }])
    }
  }
}

export const setLogin = credential => {
  return {
    type: SET_LOGIN,
    payload: credential
  }
}
