import _ from 'lodash'
import { ProposalService, StepsService } from '../services'
import { SET_PROPOSALS, SET_PROPOSAL, SET_DOCUMENTS, SET_STEPS } from '../constants/action-types'
import { Errors } from '../helpers'

export const getProposal = id => {
  return async dispatch => {
    const proposalService = new ProposalService()
    let proposal = {}

    try {
      const result = await proposalService.getById(id)
      proposal = result[0]
    } catch (error) {
      Errors.catchHttp(error, [{
        statusCode: 404,
        title: '404',
        message: 'Proposta não encontrada'
      }])
    }

    dispatch(setProposal(proposal))
  }
}

const setProposal = proposal => {
  return {
    type: SET_PROPOSAL,
    payload: proposal
  }
}

export const getDocuments = idProposal => {
  return dispatch => {
    const proposalService = new ProposalService()

    proposalService.getDocuments(idProposal)
      .then(images => {
        let promises = []

        _.forEach(images.records, (document, index) => {
          promises.push(proposalService.getDocuments(idProposal, document.id)
            .then(document => document.records[0]))
        })

        return Promise.all(promises)
      })
      .then(documents => dispatch(setDocuments(documents)))
  }
}

const setDocuments = documents => {
  return {
    type: SET_DOCUMENTS,
    payload: documents
  }
}

export const getSteps = idProposal => {
  return async dispatch => {
    const stepsService = new StepsService()

    try {
      const steps = await stepsService.getSteps(idProposal)

      dispatch(setSteps(steps.records))
    } catch (error) {
      console.log(error)
    }
  }
}

const setSteps = steps => {
  return {
    type: SET_STEPS,
    payload: steps
  }
}

export const getProposals = (offset, limit, filters) => {
  return async dispatch => {
    const proposalService = new ProposalService()
    let proposals = {}
    let queryString = ''

    try {
      filters.map((item, index) => {
        return queryString += item.queryString
      })
      proposals = await proposalService.get(`?offset=${offset}&limit=${limit}${queryString}`)
    } catch (error) {
      Errors.catchHttp(error, [{
        statusCode: 404,
        title: '404',
        message: 'Nenhuma proposta encontrada'
      }])
    }

    dispatch(setProposals(proposals, filters))
  }
}

const setProposals = (proposals, filters) => {
  return {
    type: SET_PROPOSALS,
    payload: proposals,
    filters: filters,
  }
}