import Logo from './logo.jpg'
import CustomerIcon from './customer.svg'
import SalesmanIcon from './salesman.svg'
import FinancialIcon from './financial.svg'

export {
  Logo,
  CustomerIcon,
  SalesmanIcon,
  FinancialIcon
}