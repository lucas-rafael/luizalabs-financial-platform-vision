import React from 'react';
import PropTypes from 'prop-types'
import Input from './input'

export const InputText = props => (
  <Input 
    {...props} />
)

InputText.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string
}

InputText.defaultProps = {
  value: '',
  disabled: false,
  className: ''
}
