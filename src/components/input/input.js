import React from 'react';
import PropTypes from 'prop-types'

const Input = props => (
  <input
    type={props.type}
    value={props.value}
    onChange={props.onChange}
    disabled={props.disabled} 
    className={props.className}/>
)

Input.propTypes = {
  type: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string
}

Input.defaultProps = {
  type: 'text',
  value: '',
  disabled: false,
  className: ''
}

export default Input