import React from 'react';
import PropTypes from 'prop-types'
import Input from './input'

export const InputDate = props => (
  <Input 
    type='text' 
    {...props} />
)

InputDate.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string
}

InputDate.defaultProps = {
  value: '',
  disabled: false,
  className: ''
}
