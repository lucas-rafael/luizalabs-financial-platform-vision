import React from 'react';
import PropTypes from 'prop-types'
import Input from './input'

export const InputNumber = props => (
  <Input 
    type='number' 
    {...props} />
)

InputNumber.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string
}

InputNumber.defaultProps = {
  value: '',
  disabled: false,
  className: ''
}
