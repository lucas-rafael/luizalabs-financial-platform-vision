import React, { Component } from 'react'
import EventBus from 'eventing-bus'

export class Toastr extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      messages: []
    }

    EventBus.on('luizalabs.vision.toastr', (payload) => {
      this.addMessage(payload);
    })
  }

  addMessage(message) {
    let messages = this.state.messages
    message.id = Math.random()
    messages.push(message)

    this.setState({
      messages
    })

    setTimeout(() => {
      this.removeMessage(message.id);
    }, (message.timeout || 5000) + messages.length * 500)
  }

  removeMessage(id) {
    let messages = this.state.messages
    const index = messages.findIndex((message) => message.id === id)

    if (index > -1) {
      messages.splice(index, 1)

      this.setState({
        messages
      })
    }
  }

  render() {
    return (
      <div className="toastr-container">
        {
          this.state.messages.map(message => {
            return (
              <div className={`toastr-message-container ${message.type}`} key={message.id}>
                <a className="toastr-message-close" onClick={() => this.removeMessage(message.id)}>
                  <span>&times;</span>
                </a>
                <div className="toastr-message-message-container">
                  <div className="toastr-message-title-container">
                    <span>{message.title}</span>
                  </div>
                  <div className="toastr-message-text-container">
                    <span style={{ whiteSpace: 'pre-wrap' }}>{message.text}</span>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}