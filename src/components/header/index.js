import React from 'react'
import { Link } from 'react-router-dom'
import { Logo } from '../../assets/images/'
import { ColorBar } from '../'
import { routes } from '../../constants'
import Auth from '../../security/auth'

const getUserName = () => {
  const user = Auth.getUser()
  return (user || {}).name
}

export const Header = props => {
  return (
    <div>
      <ColorBar />
      <nav className="navbar -header">
        <div className="container">
          <Link to={routes.HOME.path} className="navbar__brand">
            <img src={Logo} alt="Magazine Luiza - Administrativo" className="navbar__brand__logo" />
          </Link>
          <ul className="navbar__nav">
            
          </ul>
          <ul className="navbar__nav -right">
            <li className="dropdown -navbar -arrow -right">
              <a className="dropdown__link">{`Olá, ${getUserName()}`}</a>
              <ul className="dropdown__menu -right">
                <li>
                  <Link to={`${routes.LOGIN.path}`} className="dropdown__link">Sair</Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  )
}