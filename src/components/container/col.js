import React from 'react'
import PropTypes from 'prop-types'

export const Col = props => {
  return (
    <div className={`gr-${props.size}`}>
      {props.children}
    </div>
  )
}

Col.propTypes = {
  size: PropTypes.number.isRequired
}
