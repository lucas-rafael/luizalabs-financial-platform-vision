import React from 'react'

export const Row = props => {
  return (
    <div className='gr'>
      {props.children}
    </div>
  )
}
