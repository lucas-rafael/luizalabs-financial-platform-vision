import React from 'react'
import PropTypes from 'prop-types'

export const Panel = props => {
  return (
    <div className="panel">
      {
        props.icon
        && (
          <header className="panel__header">
            <span className='panel__icon'>
              <img src={props.icon} alt={props.subTitle}/>
            </span>
          </header>
        )
      }
      {
        props.title
        && (
          <header className="panel__header">
            <h3 className="panel__title -top-title">{props.title}</h3>
          </header>
        )
      }
      {
        props.subTitle
        && (
          <header className="panel__header">
            <h3 className={`panel__title ${(props.icon) ? 'subtitle-icon' : ''}`}>{props.subTitle}</h3>
          </header>
        )
      }
      <section className="panel__body">
        {props.children}
      </section>
    </div>
  )
}

Panel.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  icon: PropTypes.string,
  className: PropTypes.string
}