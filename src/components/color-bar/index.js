import React from 'react'

export const ColorBar = props => {
  return (
    <div className="strips">
      <span className="strips__strip -yellow"></span>
      <span className="strips__strip -orange"></span>
      <span className="strips__strip -red"></span>
      <span className="strips__strip -pink"></span>
      <span className="strips__strip -purple"></span>
      <span className="strips__strip -purple-dark"></span>
      <span className="strips__strip -blue"></span>
      <span className="strips__strip -cyan"></span>
      <span className="strips__strip -green-light"></span>
      <span className="strips__strip -green"></span>
    </div>
  )
}
