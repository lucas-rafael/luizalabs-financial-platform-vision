import React from 'react'
import { LabelText } from '../'

export const Thumbnail = props => {
  const image = props.image

  return (
    <div className={"thumbnail"}>
      <img src={`data:image/jpg;base64,${image.image}`} alt={image.type} />
      <LabelText text={`${image.type} - ${image.id}`} />
    </div>
  )
}
