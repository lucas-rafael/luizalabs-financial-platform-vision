import React, { Component } from 'react'
import { PaginationLink } from './paginationLink'
import PropTypes from 'prop-types'

export class Pagination extends Component {
    buildPages() {
        let index = 0

        index = (this.props.limit > 0) ? (this.props.offset / this.props.limit) + 1 : 1

        return {
            limit: this.props.limit,
            offset: this.props.offset,
            recordCount: this.props.recordCount,
            index: index
        }
    }

    onClick(index) {
        const limit = this.props.limit
        const offset = ((index - 1) * limit)

        this.props.onChangePage(offset, limit)
    }

    renderPrevious(pages) {
        return this.renderLink(<span>«</span>, pages.index - 1, (pages.index > 1 ? false : true))
    }

    renderNext(pages) {
        return this.renderLink(<span>»</span>, pages.index + 1, (pages.recordCount < 50 ? true : false))
    }

    renderLink(content, value, disabled) {
        return (
            <PaginationLink value={value} onClick={(index) => this.onClick(index)} disabled={disabled}>
                {content}
            </PaginationLink >
        )
    }

    render() {
        const pages = this.buildPages()

        return (
            <div className="text-center" >
                <ul className="pagination">
                    {this.renderPrevious(pages)}

                    <PaginationLink value={0} disabled>
                        <strong>{pages.index}</strong>
                    </PaginationLink>

                    {this.renderNext(pages)}
                </ul>
            </div>
        )
    }
}

Pagination.propTypes = {
    limit: PropTypes.number.isRequired,
    offset: PropTypes.number.isRequired,
    recordCount: PropTypes.number.isRequired,
    onChange: PropTypes.func
}