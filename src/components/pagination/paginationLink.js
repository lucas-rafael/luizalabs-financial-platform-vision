import React from 'react'
import PropTypes from 'prop-types'

export const PaginationLink = (props) => {
    const onClick = () => {
        if (!props.disabled) {
            props.onClick(props.value)
        }
    }

    return (
        <li className={`page-item ${props.disabled ? 'disabled' : null}`}>
            <a className="page-link" onClick={() => onClick()}>
                {props.children}
            </a>
        </li>
    )
}

PaginationLink.propTypes = {
    value: PropTypes.number.isRequired,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
}

PaginationLink.defaultProps = {
    disabled: false
}