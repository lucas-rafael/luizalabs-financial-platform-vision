import React from 'react'
import PropTypes from 'prop-types'

const Label = props => {
  return (
    <label className={props.className}>
      {props.text}
    </label>
  )
}

Label.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string
}

export default Label