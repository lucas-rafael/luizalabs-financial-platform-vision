import React from 'react'
import PropTypes from 'prop-types'
import Label from './label'

export const LabelText = props => {
  return (
    <Label {...props}/>
  )
}

LabelText.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string
}

LabelText.defaultProps = {
  className: 'label-text'
}
