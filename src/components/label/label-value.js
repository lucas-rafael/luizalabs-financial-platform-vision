import React from 'react'
import PropTypes from 'prop-types'
import Label from './label'

export const LabelValue = props => {
  return (
    <Label {...props} />
  )
}

LabelValue.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string
}

LabelValue.defaultProps = {
  className: 'label-value'
}