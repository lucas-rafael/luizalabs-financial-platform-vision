import React from 'react'
import PropTypes from 'prop-types'
import Label from './label'

export const LabelStrong = props => {
  return (
    <Label {...props} />
  )
}

LabelStrong.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string
}

LabelStrong.defaultProps = {
  className: 'label-strong'
}