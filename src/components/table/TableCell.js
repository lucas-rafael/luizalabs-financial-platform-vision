import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableCell = (props) => {
  const classes = classNames('table__cell', props.className);
  return (
    <td className={classes}>
      {props.children}
    </td>
  )
};

TableCell.propTypes = {
  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};

export default TableCell;
