import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import TableBody from './TableBody';
import TableCell from './TableCell';
import TableHead from './TableHead';
import TableHeadCell from './TableHeadCell';
import TableRow from './TableRow';

export const Table = (props) => {
  const classes = classNames('table', { '-striped': props.striped });

  return (
    <table className={classes}>
      {props.children}
    </table>
  );
};

Table.propTypes = {
  /** Condição para definir se a tabela terá o aspecto zebrado */
  striped: PropTypes.bool,

  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};

Table.Body = TableBody;
Table.Cell = TableCell;
Table.Head = TableHead;
Table.HeadCell = TableHeadCell;
Table.Row = TableRow;