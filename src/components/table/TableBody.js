import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableBody = (props) => {
  const classes = classNames('table__body', props.className);
  return (
    <tbody className={classes}>
      {props.children}
    </tbody>
  );
};

TableBody.propTypes = {
  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};

export default TableBody;
