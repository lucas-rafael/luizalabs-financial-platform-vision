import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableHead = (props) => {
  const classes = classNames('table__head', props.className);
  return (
    <thead className={classes}>
      {props.children}
    </thead>
  )
};


TableHead.propTypes = {
  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};

export default TableHead;
