import React from 'react';
import PropTypes from 'prop-types';

import Table from './Table';

const TableGroup = (props) => {
  const renderHead = () => props.head.map((h, i) =>
    <Table.HeadCell key={i.toString()}>{h}</Table.HeadCell>
  );
  const renderRows = () => props.rows.map((row, index) => 
    <Table.Cell key={index.toString()}>{row}</Table.Cell>
  );

  return (
    <Table {...props}>
      <Table.Head>
        <Table.Row>
          {renderHead()}
        </Table.Row>
      </Table.Head>
      <Table.Body>
        <Table.Row Body>
          {renderRows()}
        </Table.Row>
      </Table.Body>
    </Table>
  );
};

TableGroup.propTypes = {
  head: PropTypes.arrayOf(PropTypes.string).isRequired,
  rows: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default TableGroup;
