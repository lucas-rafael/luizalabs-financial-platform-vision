import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableRow = (props) => {
  const classes = classNames('table__row', props.className);
  return (
    <tr className={classes}>
      {props.children}
    </tr>
  )
};

TableRow.propTypes = {
  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};

export default TableRow;
