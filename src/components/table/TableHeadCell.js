import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TableHeadCell = (props) => {
  const classes = classNames('table__head__cell', props.className);
  return (
    <th className={classes}>
      {props.children}
    </th>
  )
};

TableHeadCell.propTypes = {
  /** Conteúdo primário. */
  children: PropTypes.any,

  /** Classes CSS adicionais. */
  className: PropTypes.string
};


export default TableHeadCell;
