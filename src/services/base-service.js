import axios from 'axios'

export default class BaseService {
  constructor (config, path) {
    this.config = config
    this.path = path
  }

  get(params = '') {
    return axios.get(`${this.config.url}${this.path}${params}`, {
      headers: {
        'Authorization': this.config.token
      }
    })
      .then(result => result.data)
  }

  getById(id) {
    return axios.get(`${this.config.url}${this.path}/${id}`, {
      headers: {
        'Authorization': this.config.token
      }
    })
      .then(result => result.data.records)
  }

  post(params = {}) {
    return axios.post(`${this.config.url}${this.path}`, params, {
      headers: {
        'Authorization': this.config.token
      }
    })
      .then(result => result)
  }
}