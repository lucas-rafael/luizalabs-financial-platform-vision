import BaseService from './base-service';
import config from '../config'

export class ProposalService extends BaseService {
  constructor () {
    super(config.cdc.api, '/proposals')
  }

  getDocuments(idProposal, idDocument = null) {
    if (idDocument) {
      return this.get(`/${idProposal}/documents/${idDocument}`)
    }
    return this.get(`/${idProposal}/documents`)
  }
}