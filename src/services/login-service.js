import BaseService from './base-service';
import config from '../config'

export class LoginService extends BaseService {
  constructor () {
    super(config.cdc.apiLogin, '/users/login')
  }

  login(credential) {
    return this.post(credential)
  }
}