import BaseService from './base-service';
import config from '../config'

export class StepsService extends BaseService {
  constructor () {
    super(config.cdc.apiAudit, '/proposals')
  }

  getSteps(idProposal) {
    return this.get(`/${idProposal}/audits`)
  }
}