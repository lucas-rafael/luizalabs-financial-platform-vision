
export default {
  cdc: {
    api: {
      url: 'http://avengers-app.appsluiza.com.br:9000/v2/tonystark', //process.env.CDC_PROPOSAL_API_URL,
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiUGxhdGFmb3JtYSBGaW5hbmNlaXJhIn0.bSCB1k2cvqnppLvjAes9-l1MjAnoJpGEDq6lCHvX0cw'//process.env.CDC_PROPOSAL_API_TOKEN
    },
    apiAudit: {
      url: 'http://avengers-app.appsluiza.com.br:9001/v2/hawkeye',
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiUGxhdGFmb3JtYSBGaW5hbmNlaXJhIn0.bSCB1k2cvqnppLvjAes9-l1MjAnoJpGEDq6lCHvX0cw'
    },
    apiLogin: {
      url: 'http://192.168.50.143:9000/v1/luizalabs-financial-platform-vision-api',
      token: ''
    }
  }
}