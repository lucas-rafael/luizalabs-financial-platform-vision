import cookie from 'react-cookies'

export default class Auth {
    static isLoggedIn() {
        const user = this.getUser()
        return user !== null && user !== undefined 
    }

    static getUser() {
        const user = cookie.load('luizalabs.vision.user')
        return user
    }

    static logIn(user) {
        const expires = new Date(Date.now() + user.authorization.expiration)
        cookie.save('luizalabs.vision.user', user, { path: '/', expires })
    }

    static logOut() {
        cookie.remove('luizalabs.vision.user', { path: '/' })
    }
}