import React from 'react'
import { Provider } from 'react-redux'
import { AnimatedSwitch } from 'react-router-transition'
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import { routes } from './constants'
import store from './reducers'
import Index from './container'
import Auth from './security/auth'

export default App => {  
  return (
    <Provider store={store}>
      <BrowserRouter>
        <AnimatedSwitch
          atEnter={{ opacity: 0 }}
          atLeave={{ opacity: 0 }}
          atActive={{ opacity: 1 }}
          className="switch-wrapper">
          <Route path={routes.LOGIN.path} component={routes.LOGIN.component} />
          <Index>
            {
              Object.keys(routes).map((route, index) =>
                <Route key={index} exact={true} path={routes[route].path} component={
                  (!routes[route].auth || (routes[route].auth && Auth.isLoggedIn())) ? routes[route].component : () => <Redirect to='/login' />
                } />
              )
            }
          </Index>
          <Redirect to={routes.PROPOSAL_LIST.path} />
        </AnimatedSwitch>
      </BrowserRouter>
    </Provider>
  )
}
