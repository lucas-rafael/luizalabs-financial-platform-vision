import _ from 'lodash'
import { Toastr } from "./index"

export class Errors {
  static getHttpBehavior(behaviors, status) {
    let behavior = _.find(behaviors, b => b.statusCode === status)

    if (!behavior) {
      behavior = _.find(defaultBehaviors, b => b.statusCode === status)
    }

    return behavior
  }

  static catchHttp(error, behaviors = []) {
    if (error.response && error.response.status) {
      const behavior = this.getHttpBehavior(behaviors, error.response.status)
      if (behavior.statusCode < 500) {
        Toastr.warning(behavior.title, behavior.message)
      } else {
        Toastr.error(behavior.title, behavior.message)
      }
    } else {
      const behavior = _.last(defaultBehaviors)
      Toastr.error(behavior.title, behavior.message)
    }

    console.log(error)
  }
}

const defaultBehaviors = [{
  statusCode: 400,
  title: 'Erro na validação',
  message: 'Preencha todas as informações'
}, {
  statusCode: 401,
  title: 'Não autorizado',
  message: 'Operação não autorizada no servidor'
}, {
  statusCode: 404,
  title: 'Não encontrado',
  message: 'Nenhum registro encontrado'
}, {
  statusCode: 500,
  title: 'Erro inesperado',
  message: 'Ocorreu um erro inesperado'
}, {
  statusCode: 503,
  title: 'Servidor indisponível',
  message: 'O servidor está indisponível no momento'
}, {
  statusCode: 504,
  title: 'Limite de espera atingido',
  message: 'O tempo de espera da operação foi atingido'
}, {
  title: 'Erro inesperado!',
  message: 'Ocorreu um erro inesperado. Tente novamente'
}]