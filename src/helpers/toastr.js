import EventBus from 'eventing-bus'
import { status } from '../constants'

const defaultTimeout = 5000

export class Toastr {
  static success(title, text, timeout) {
    const config = {
      type: status.getName(status.SUCCESS),
      title,
      text,
      timeout: timeout || defaultTimeout
    }

    this.push(config)
  }

  static info(title, text, timeout) {
    const config = {
      type: status.getName(status.INFORMATION),
      title,
      text,
      timeout: timeout || defaultTimeout
    }

    this.push(config)
  }

  static warning(title, text, timeout) {
    const config = {
      type: status.getName(status.WARNING),      
      title,
      text,
      timeout: timeout || defaultTimeout
    }

    this.push(config)
  }

  static error(title, text, timeout) {
    const config = {
      type: status.getName(status.ERROR),
      title,
      text,
      timeout: timeout || defaultTimeout
    }

    this.push(config)
  }

  static push(config) {
    EventBus.publish('luizalabs.vision.toastr', {
      ...config
    })
  }
}