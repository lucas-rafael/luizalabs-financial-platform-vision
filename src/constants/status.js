export const status = {
  SUCCESS: 0,
  INFORMATION: 1,
  WARNING: 2,
  ERROR: 4,
  getName: function (status) {
    let name = '';

    switch (status) {
      case this.SUCCESS:
        name = 'success';
        break;
      case this.INFORMATION:
        name = 'info';
        break;
      case this.WARNING:
        name = 'warning';
        break;
      case this.ERROR:
        name = 'danger';
        break;
      default:
        break;
    }

    return name;
  }
};