export const typeSearch = {
    ID_PROPOSAL: 'idProposal',
    STATUS_PROPOSAL: 'statusProposal',
    CPF_CUSTUMER: 'cpfCustumer',
    ID_FINANCIAL: 'idFinancial',
}