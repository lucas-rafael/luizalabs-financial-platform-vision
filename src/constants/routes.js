import ProposalList from '../container/proposal/list'
import ProposalDetail from '../container/proposal/detail'
import Login from '../container/login'

export const routes = {
  HOME: {
    path: '/',
    component: ProposalList,
    auth: true
  },
  PROPOSAL_LIST: {
    path: '/cdc/proposals',
    component: ProposalList,
    auth: true
  },
  PROPOSAL_DETAIL: {
    path: '/cdc/proposals/:id',
    component: ProposalDetail,
    auth: true
  },
  LOGIN: {
    path: '/login',
    component: Login,
    auth: false
  }
};