import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assets/proa/css/proa.css'
import './assets/styles/style.css'
import './assets/styles/toastr.css'

ReactDOM.render(<App />, document.getElementById('root'))
